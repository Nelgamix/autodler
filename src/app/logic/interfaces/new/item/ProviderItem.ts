import {Item} from './Item';

export interface ProviderItem extends Item {
  extension: string;
  mediaPath: string;
  parent: string;
}
