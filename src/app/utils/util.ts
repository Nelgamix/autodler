export function arrayLast<T>(array: T[]): T | undefined {
  return array.length > 0 ? array[array.length - 1] : undefined;
}

export function completeObject<T, U extends T>(dest: T, source: Pick<U, Exclude<keyof U, keyof T>>): T & Pick<U, Exclude<keyof U, keyof T>> {
  return Object.assign<T, Pick<U, Exclude<keyof U, keyof T>>>(dest, source);
}

export type ExcludeInType<T, K extends T> = Pick<K, Exclude<keyof K, keyof T>>;
