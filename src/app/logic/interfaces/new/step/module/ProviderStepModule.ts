import {StepModule} from './StepModule';
import {ProviderStep} from '../ProviderStep';
import {ContainerItem} from '../../item/ContainerItem';
import {Subject} from 'rxjs';
import {ProviderStepItem, ProviderStepItems} from '../item/ProviderStepItems';
import {ProviderItem} from '../../item/ProviderItem';

export abstract class ProviderStepModule<M> extends StepModule<M, ContainerItem, ProviderStepItems> implements ProviderStep {
  protected readonly itemFolder = 'provider';

  protected createStepItem(): ProviderStepItems {
    return {
      items: [],
      run: null,
      updates: new Subject<void>(),
      progress: 0,
    };
  }

  protected createProviderStepItem(): ProviderStepItem {
    return {
      item: null,
      run: null,
      updates: new Subject<void>(),
      progress: 0,
      data: null,
    };
  }

  protected createProviderItem(): ProviderItem {
    return null;
  }
}
