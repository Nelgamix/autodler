import {StepModule} from './StepModule';
import {ContainerStep} from '../ContainerStep';
import {SourceItem} from '../../item/SourceItem';
import {Subject} from 'rxjs';
import {ContainerStepItems} from '../item/ContainerStepItems';

export abstract class ContainerStepModule<M> extends StepModule<M, SourceItem, ContainerStepItems> implements ContainerStep {
  protected readonly itemFolder = 'container';

  protected createStepItem(): ContainerStepItems {
    return {
      items: [],
      run: null,
      updates: new Subject<void>(),
      progress: 0,
    };
  }
}
