export enum DownloadModuleWeight {
  Minimal,
  Low,
  Normal,
  High,
  Critical,
}
