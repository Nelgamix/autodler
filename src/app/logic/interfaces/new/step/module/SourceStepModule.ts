import {StepModule} from './StepModule';
import {SourceStep} from '../SourceStep';
import {Index} from '../../Index';
import {SourceItem} from '../../item/SourceItem';
import {ItemStatus} from '../../item/Item';
import {AppService} from '../../../../../providers/app.service';
import {SourceStepItems} from '../item/SourceStepItems';
import {Context} from '../../Context';
import {SourceConfiguration} from '../../configuration/SourceConfiguration';
import {completeObject} from '../../../../../utils/util';
import {StepItem} from '../item/StepItem';

export abstract class SourceStepModule<M, I extends SourceConfiguration> extends StepModule<M, I, SourceStepItems> implements SourceStep {
  protected readonly indexName = 'index.json';
  protected readonly itemFolder = 'source';

  protected constructor(app: AppService, context: Context, input: I) {
    super(app, context, input);
  }

  public createIndex(): Index {
    return {
      path: this.getIndexPath(),
      type: this.definition.type,
      name: 'New index',
      id: '',
      source: {items: []},
      container: {items: []},
      provider: {items: []},
    };
  }

  protected createConfiguration(): SourceConfiguration {
    return {
      path: '',
      type: this.definition.type,
      name: '',
      id: '',
      configuration: {
        folder: '',
      },
    };
  }

  protected createStepItem(): SourceStepItems {
    return completeObject<StepItem, SourceStepItems>(super.createStepItem(), {
      items: [],
    });
  }

  protected createSourceItem(params: {
    name?: string,
    id?: string,
    title?: string,
    url?: string,
  } = {}): SourceItem {
    return {
      path: this.getItemPath(),
      type: this.definition.type,
      name: params.name || '',
      id: params.id || '',
      title: params.title || '',
      url: params.url || '',
      information: {
        marked: true,
        created: Date.now(),
      },
      status: ItemStatus.Standby,
      child: [],
    };
  }

  protected getIndexPath(): string {
    return this.context.configuration.folder + '/' + this.indexName;
  }
}
