import { Injectable } from '@angular/core';
import {StandaloneModelService} from './StandaloneModelService';
import {ContainerItem} from '../../logic/interfaces/new/item/ContainerItem';
import {ElectronService} from '../electron.service';

@Injectable({
  providedIn: 'root'
})
export class ContainerItemService extends StandaloneModelService<ContainerItem> {
  constructor(electron: ElectronService) {
    super(electron);
  }
}
