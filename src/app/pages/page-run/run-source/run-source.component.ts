import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SourcesService} from '../../../providers/sources.service';
import {ElectronService} from '../../../providers/electron.service';
import {remove} from 'lodash';
import {Index} from '../../../logic/interfaces/generic';
import {Config} from '../../../logic/interfaces/generic/Config';
import {Credentials} from '../../../logic/interfaces/generic/Credentials';
import {SourceItems, SourceItemsRunState} from '../../../logic/interfaces/application/source/SourceItems';

@Component({
  selector: 'app-run-source',
  templateUrl: './run-source.component.html',
  styleUrls: ['./run-source.component.scss']
})
export class RunSourceComponent implements OnInit {
  @Input() index: Index;
  @Input() config: Config;
  @Input() credentials: Credentials;
  @Input() allowedRun: boolean;
  sourceItems: SourceItems;
  running = false;
  @Output() runStarted = new EventEmitter<void>();
  @Output() runFinished = new EventEmitter<SourceItems>();

  constructor(
    private sourcesService: SourcesService,
    private electronService: ElectronService,
  ) { }

  ngOnInit() {
  }

  run(): void {
    if (!this.canRun) {
      return;
    }

    this.reset();

    this.running = true;
    this.sourceItems = this.sourcesService.run(this.config, this.credentials, this.index);
    this.sourceItems.run.start.subscribe((state: SourceItemsRunState) => {
      if (state === SourceItemsRunState.Finished) {
        this.running = false;
        this.removeDownloadedItems();
        this.runFinished.emit(this.sourceItems);
      }
    }, (err) => {
      console.error(err);
      this.running = false;
      this.runFinished.emit(this.sourceItems);
    });

    this.runStarted.emit();
  }

  selectAll(): void {
    this.sourceItems.output.forEach(i => i.downloadInfo.marked = true);
  }

  selectNone(): void {
    this.sourceItems.output.forEach(i => i.downloadInfo.marked = false);
  }

  openBrowser(url: string): void {
    if (this.electronService.isElectron()) {
      this.electronService.shell.openExternal(url);
    }
  }

  removeDownloadedItems(): void {
    remove(this.sourceItems.output, i => this.index.items.success.find(ip => ip === i.id));
  }

  reset(): void {
    this.sourceItems = null;
    this.running = false;
  }

  get canRun(): boolean {
    return this.allowedRun && !this.running;
  }
}
