export interface Step<O> {
  createOutput(): O;
}
