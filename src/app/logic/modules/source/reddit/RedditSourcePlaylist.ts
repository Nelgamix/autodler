export enum RedditSourcePlaylist {
  Liked = 'liked',
  Saved = 'saved',
}
