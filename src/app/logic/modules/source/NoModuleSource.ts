import {SourceStepModule} from '../../interfaces/new/step/module/SourceStepModule';
import {StepModulePriority} from '../../interfaces/new/step/module/StepModulePriority';
import {SourceConfiguration} from '../../interfaces/new/configuration/SourceConfiguration';
import {SourceStepModuleDefinition} from '../../interfaces/new/step/module/definition/SourceStepModuleDefinition';

export class NoModuleSource extends SourceStepModule<NoModuleSource, SourceConfiguration> {
  public static definition: SourceStepModuleDefinition<NoModuleSource, SourceConfiguration> = {
    type: 'no-module',
    priority: StepModulePriority.NoModule,
    instantiate: (app, context, input) => new NoModuleSource(app, context, input),
  };
}
