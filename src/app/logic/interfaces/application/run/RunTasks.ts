import {Observable, Subscriber} from 'rxjs';
import {remove} from 'lodash';

export class RunTasks {
  private tasks: Observable<void>[];
  private tasksWaiting: Observable<void>[];
  private tasksRunning: Observable<void>[];
  private tasksFinished: Observable<void>[];

  private subscriber: Subscriber<void>;
  private maxConcurrentTasks = 5;

  constructor(tasks: Observable<void>[]) {
    this.tasks = tasks;
    this.tasksWaiting = this.tasks.slice();
  }

  public run(): Observable<void> {
    const obs = new Observable<void>(subscriber => this.subscriber = subscriber);
    this.launchTask();
    return obs;
  }

  private launchTask(): void {
    while (this.tasksWaiting.length > 0 && this.tasksRunning.length < this.maxConcurrentTasks) {
      const task = this.tasksWaiting.splice(0, 1)[0];
      this.tasksRunning.push(task);
      task.subscribe(() => this.finishTask(task));
    }

    if (this.tasksWaiting.length === 0 && this.tasksRunning.length === 0) {
      this.subscriber.next();
      this.subscriber.complete();
    }
  }

  private finishTask(task: Observable<void>): void {
    remove(this.tasksWaiting, t => t === task);
    this.tasksFinished.push(task);
    this.launchTask();
  }
}
