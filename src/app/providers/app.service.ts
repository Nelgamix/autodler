import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, mapTo, tap} from 'rxjs/operators';
import {remove} from 'lodash';
import {SourceItemService} from './model/source-item.service';
import {WorkspaceService} from './model/workspace.service';
import {ContainerItemService} from './model/container-item.service';
import {ProviderItemService} from './model/provider-item.service';
import {IndexService} from './model/index.service';
import {SourceConfigurationService} from './model/source-configuration.service';
import {SourceCredentialsService} from './model/source-credentials.service';
import {Workspace} from '../logic/interfaces/new/Workspace';

const nameOpenWorkspaces = 'open-workspaces';
const nameOpenWorkspace = 'open-workspace';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  openWorkspaces: string[];
  workspace: Workspace;

  constructor(
    public workspaceService: WorkspaceService,
    public indexService: IndexService,
    public sourceConfigurationService: SourceConfigurationService,
    public sourceCredentialsService: SourceCredentialsService,
    public sourceItemService: SourceItemService,
    public containerItemService: ContainerItemService,
    public providerItemService: ProviderItemService,
  ) {
  }

  initialise(): Observable<void> {
    const openWorkspaces = localStorage.getItem(nameOpenWorkspaces);
    const openWorkspace = localStorage.getItem(nameOpenWorkspace);

    if (openWorkspaces) {
      this.openWorkspaces = JSON.parse(openWorkspaces);
    }

    if (openWorkspace) {
      return this.workspaceService.loadByPath(openWorkspace).pipe(
        catchError(() => of(undefined)),
        tap((workspace) => this.workspace = workspace),
        mapTo(undefined),
      );
    }

    return of();
  }

  openWorkspace(path: string): Observable<Workspace> {
    return this.workspaceService.loadByPath(path).pipe(
      tap((workspace) => {
        this.workspace = workspace;
        this.addWorkspace(workspace);
      }),
    );
  }

  closeWorkspace(): void {
    remove(this.openWorkspaces, (w) => w === this.workspace.path);
    this.workspace = undefined;
  }

  addWorkspace(workspace: Workspace): void {
    if (!workspace || this.openWorkspaces.find(w => w === workspace.path)) {
      return;
    }

    this.openWorkspaces.push(workspace.path);
  }
}
