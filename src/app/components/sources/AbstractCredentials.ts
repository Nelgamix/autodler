import {EventEmitter} from '@angular/core';
import {Credentials} from '../../logic/interfaces/generic/Credentials';

export interface AbstractCredentials {
  // Credentials sent into input to modify
  credentials: Credentials;
  // Emitter that sends boolean describing credentials validity
  validityChanges: EventEmitter<boolean>;

  // Creates a new credentials or resets to default
  resetCredentials(): void;
  // Gets the current credentials
  getCredentials(): Credentials;
  // Sets the credentials
  setCredentials(credentials: Credentials): void;
}
