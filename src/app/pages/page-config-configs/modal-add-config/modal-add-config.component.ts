import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalRef} from 'ng-zorro-antd';
import {ElectronService} from '../../../providers/electron.service';
import {sanitizePath} from '../../../utils/path';
import {Config} from '../../../logic/interfaces/generic/Config';

@Component({
  selector: 'app-modal-add-config',
  templateUrl: './modal-add-config.component.html',
  styleUrls: ['./modal-add-config.component.scss']
})
export class ModalAddConfigComponent implements OnInit {
  @Input() config: Config;

  form: FormGroup;
  types = [['Reddit', 'reddit']];

  constructor(
    private modal: NzModalRef,
    private fb: FormBuilder,
    private electronService: ElectronService,
  ) { }

  ngOnInit() {
    if (!this.config) {
      this.config = {
        name: '',
        type: '',
        paths: {
          items: '',
          medias: '',
        },
      };
    }

    this.form = this.fb.group({
      name: [this.config.name, [Validators.required]],
      type: [this.config.type, [Validators.required]],
      pathItems: [this.config.paths.items, [Validators.required]],
      pathMedias: [this.config.paths.medias, [Validators.required]],
    });
  }

  handleCancel(): void {
    this.modal.close();
  }

  handleOk(): void {
    if (!this.form.valid) {
      return;
    }

    this.copyFormInConfig();

    this.modal.close(this.config);
  }

  chooseFileConfig(): void {
    this.electronService.dialog.showOpenDialog(null, {
      filters: [
        {name: 'JSON', extensions: ['json']},
      ],
      properties: ['openFile'],
    }).then((res) => {
      if (res && !res.canceled && res.filePaths.length > 0) {
        const path = sanitizePath(res.filePaths[0]);

        this.electronService.existsFile(path).subscribe((fileExists: boolean) => {
          if (fileExists) {
            // Import
            this.electronService.readFile(path).subscribe((file) => {
              this.copyConfigInForm(JSON.parse(file));
            });
          }
        });
      }
    });
  }

  chooseDirectory(controlName: string): void {
    this.electronService.dialog.showOpenDialog(null, {
      properties: ['openDirectory', 'promptToCreate'],
    }).then((res) => {
      if (res && !res.canceled && res.filePaths.length > 0) {
        const path = sanitizePath(res.filePaths[0]);

        this.form.controls[controlName].patchValue(path);

        if (controlName === 'pathMedias') {
          this.form.controls['pathItems'].patchValue(path + '/items');
        }
      }
    });
  }

  copyConfigInForm(config: Config): void {
    this.config = config;

    this.form.controls['name'].patchValue(config.name);
    this.form.controls['type'].patchValue(config.type);
    this.form.controls['pathItems'].patchValue(config.paths.items);
    this.form.controls['pathMedias'].patchValue(config.paths.medias);
  }

  copyFormInConfig(): void {
    this.config.name = this.form.controls['name'].value;
    this.config.type = this.form.controls['type'].value;
    this.config.paths.items = this.form.controls['pathItems'].value;
    this.config.paths.medias = this.form.controls['pathMedias'].value;
  }
}
