import {StandaloneModel} from './StandaloneModel';

export interface Workspace extends StandaloneModel {
  source: {
    configuration: string[];
    credentials: string[];
    associations: {
      configuration: string;
      credentials: string;
    }[];
  };
}
