import {Item} from '../../generic/Item';
import {SourceItems} from '../source/SourceItems';
import {DownloadItemModule} from './DownloadItemModule';
import {DownloadModuleWeight} from './DownloadModuleWeight';

export interface DownloadItemModuleDefinition {
  name: string;
  description: string;
  type: string;
  weight: DownloadModuleWeight;
  instantiate: (item: Item, input: SourceItems) => DownloadItemModule;
  matches: (item: Item, input: SourceItems) => boolean;
}
