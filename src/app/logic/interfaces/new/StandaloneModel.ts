export interface StandaloneModel {
  path: string;
  id: string;
  type: string;
  name: string;
}
