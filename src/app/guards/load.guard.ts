import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivateChild} from '@angular/router';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../providers/app.service';

@Injectable({
  providedIn: 'root'
})
export class LoadGuard implements CanActivate, CanActivateChild {
  constructor(
    private app: AppService,
    private router: Router,
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.app.initialise().pipe(
      map((workspaceExists) => {
        console.log('[LoadGuard.canActivate]', state.url, workspaceExists);
        if (state.url !== '/config-workspace') {
          return this.router.parseUrl('/config-workspace');
        }

        return true;
      }),
    ).toPromise();
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.app.initialise().pipe(
      map((workspaceExists) => {
        console.log('[LoadGuard.canActivateChild]', state.url, workspaceExists);
        if (state.url !== '/config-workspace') {
          return this.router.parseUrl('/config-workspace');
        }

        return true;
      }),
    );
  }
}
