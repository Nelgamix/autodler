import {Component, Input, OnInit} from '@angular/core';
import {NzModalRef} from 'ng-zorro-antd';
import {sanitizePath} from '../../../utils/path';
import {ElectronService} from '../../../providers/electron.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WorkspaceService} from '../../../providers/workspace.service';
import {Workspace} from '../../../logic/interfaces/generic/Workspace';

@Component({
  selector: 'app-modal-add-workspace',
  templateUrl: './modal-add-workspace.component.html',
  styleUrls: ['./modal-add-workspace.component.scss']
})
export class ModalAddWorkspaceComponent implements OnInit {
  @Input() workspace: Workspace;

  form: FormGroup;

  constructor(
    private modal: NzModalRef,
    private electronService: ElectronService,
    private workspaceService: WorkspaceService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    if (!this.workspace) {
      this.workspace = {
        path: '',
        credentials: [],
        configs: [],
      };
    }

    this.form = this.fb.group({
      path: [this.workspace.path, [Validators.required, Validators.pattern(/.*\.json$/)]],
    });
  }

  handleCancel(): void {
    this.modal.close();
  }

  handleOk(): void {
    if (!this.form.valid) {
      return;
    }

    this.workspace.path = this.form.controls['path'].value;

    /*const changed = this.workspaceService.addWorkspace(this.workspace);

    if (changed) {
      this.workspaceService.saveWorkspace(this.workspace).subscribe();
    }*/

    this.modal.close(this.workspace);
  }

  chooseFile(): void {
    this.electronService.dialog.showOpenDialog(null, {
      filters: [
        {name: 'JSON', extensions: ['json']},
      ],
      properties: ['openFile'],
    }).then((res) => {
      if (res && !res.canceled && res.filePaths.length > 0) {
        const path = sanitizePath(res.filePaths[0]);
        this.electronService.existsFile(path).subscribe((fileExists: boolean) => {
          if (fileExists) {
            // Import
            this.electronService.readFile(path).subscribe((file) => {
              this.workspace = JSON.parse(file);
              this.form.controls['path'].patchValue(path);
            });
          } else {
            // File is a new one
            this.form.controls['path'].patchValue(path);
          }
        });
      }
    });
  }
}
