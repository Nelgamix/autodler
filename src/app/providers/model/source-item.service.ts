import { Injectable } from '@angular/core';
import {StandaloneModelService} from './StandaloneModelService';
import {SourceItem} from '../../logic/interfaces/new/item/SourceItem';
import {ElectronService} from '../electron.service';

@Injectable({
  providedIn: 'root'
})
export class SourceItemService extends StandaloneModelService<SourceItem> {
  constructor(electron: ElectronService) {
    super(electron);
  }
}
