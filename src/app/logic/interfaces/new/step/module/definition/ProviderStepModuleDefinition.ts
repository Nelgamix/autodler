import {StepModuleDefinition} from './StepModuleDefinition';
import {ContainerItem} from '../../../item/ContainerItem';

export interface ProviderStepModuleDefinition<T> extends StepModuleDefinition<T, ContainerItem> {
}
