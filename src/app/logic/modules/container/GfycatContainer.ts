import {ContainerStepModule} from '../../interfaces/new/step/module/ContainerStepModule';
import {SourceItem} from '../../interfaces/new/item/SourceItem';
import {AppService} from '../../../providers/app.service';
import {StepModulePriority} from '../../interfaces/new/step/module/StepModulePriority';
import {Context} from '../../interfaces/new/Context';
import {ContainerStepModuleDefinition} from '../../interfaces/new/step/module/definition/ContainerStepModuleDefinition';

const matcher: RegExp = /^https?:\/\/(www\.)?gfycat\.com\/([a-z]*\/)?[a-zA-Z]+\/?(\?.*)?$/;
const pattern: RegExp = /<source src="([0-9a-zA-Z\/.:]+)" type="video\/mp4"\/>/i;

/**
 * Gfycat links are, most of the time, links without the extension (.mp4),
 * and without the correct uppercase letters. To find the real media link
 * where to download the video, we must query the initial given url,
 * then search the media url in the page sent. Then, we can download
 * the video like any other video.
 */
export class GfycatContainer extends ContainerStepModule<GfycatContainer> {
  public static definition: ContainerStepModuleDefinition<GfycatContainer> = {
    name: 'Gfycat',
    description: 'Container for Gfycat links.',
    type: 'gfycat',
    priority: StepModulePriority.Normal,
    accepts: (input: SourceItem) => matcher.test(input.url),
    instantiate: (app: AppService, context: Context, input: SourceItem) => new GfycatContainer(app, context, input),
  };

  public definition = GfycatContainer.definition;

  /*public static definition: DownloadItemModuleDefinition = {
    name: 'Gfycat',
    type: 'gfycat',
    description: 'Downloads Gfycat GIFs.',
    weight: DownloadModuleWeight.Normal,
    instantiate: (item: Item, input: SourceItems) => new GfycatContainer(item, input),
    matches: (item: Item, input: SourceItems) => matcher.test(item.url),
  };

  protected definition: DownloadItemModuleDefinition = GfycatContainer.definition;

  protected constructor(item: Item, input: SourceItems) {
    super(item, input);
    this.initAnalyse();
  }

  analyse(): Observable<DownloadItemAnalyseState> {
    return new Observable<DownloadItemAnalyseState>(subscriber => {
      Axios.get(this.item.url).then((res: AxiosResponse) => {
        if (res.status !== 200) {
          throw new Error('Gfycat sent an unexpected page (code: ' + res.status + ')');
        }

        const mg = pattern.exec(res.data);
        if (!mg || mg.length <= 1) {
          throw new Error('Could not find media URL in Gfycat page.');
        }

        const mediaUrl = mg[1];

        const media = this.constructDownloadMedia(
          this.constructMedia({
            url: this.item.url,
            mediaUrl,
            extension: 'mp4',
          })
        );

        this.initRun(media);

        subscriber.next(DownloadItemAnalyseState.Finished);
        subscriber.complete();
      });
    });
  }

  run(downloadMedia: DownloadMedia): Observable<DownloadMediaRunState> {
    return this.basicRun(downloadMedia);
  }*/
}
