import 'reflect-metadata';
import '../polyfills';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
// Electron
import {ElectronService} from './providers/electron.service';
// Other
import {AppComponent} from './app.component';
import {SharedModule} from './shared.module';
// Pages
import {PageConfigWorkspaceComponent} from './pages/page-config-workspace/page-config-workspace.component';
import {PageConfigConfigsComponent} from './pages/page-config-configs/page-config-configs.component';
import {PageConfigCredentialsComponent} from './pages/page-config-credentials/page-config-credentials.component';
import {PageViewComponent} from './pages/page-view/page-view.component';
import {PageRunComponent} from './pages/page-run/page-run.component';
// Reddit
import {RedditConfigComponent} from './components/sources/reddit/reddit-config/reddit-config.component';
import {RedditCredentialsComponent} from './components/sources/reddit/reddit-credentials/reddit-credentials.component';
// Submodules
import {PageRunModule} from './pages/page-run/page-run.module';
import { ModalAddWorkspaceComponent } from './pages/page-config-workspace/modal-add-workspace/modal-add-workspace.component';
import { ModalAddConfigComponent } from './pages/page-config-configs/modal-add-config/modal-add-config.component';
import { ModalAddCredentialsComponent } from './pages/page-config-credentials/modal-add-credentials/modal-add-credentials.component';

@NgModule({
  declarations: [
    AppComponent,
    // Pages
    PageConfigWorkspaceComponent,
    PageConfigConfigsComponent,
    PageConfigCredentialsComponent,
    PageViewComponent,
    PageRunComponent,
    // Reddit
    RedditConfigComponent,
    RedditCredentialsComponent,
    // Modal
    ModalAddWorkspaceComponent,
    ModalAddConfigComponent,
    ModalAddCredentialsComponent,
  ],
  imports: [
    SharedModule,
    PageRunModule,
    AppRoutingModule,
  ],
  providers: [
    ElectronService,
  ],
  entryComponents: [
    ModalAddWorkspaceComponent,
    ModalAddConfigComponent,
    ModalAddCredentialsComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
