import {Component, OnInit, ViewChild} from '@angular/core';
import {RunSourceComponent} from './run-source/run-source.component';
import {RunDownloadComponent} from './run-download/run-download.component';
import {AppService} from '../../providers/app.service';
import {Workspace} from '../../logic/interfaces/generic/Workspace';
import {Config} from '../../logic/interfaces/generic/Config';
import {Credentials} from '../../logic/interfaces/generic/Credentials';
import {Index} from '../../logic/interfaces/generic';
import {SourceItems} from '../../logic/interfaces/application/source/SourceItems';
import {DownloadItem, DownloadItems, DownloadMedia} from '../../logic/interfaces/application/download/DownloadItems';

@Component({
  selector: 'app-page-run',
  templateUrl: './page-run.component.html',
  styleUrls: ['./page-run.component.scss']
})
export class PageRunComponent implements OnInit {
  @ViewChild('runSourceComponent', {static: false}) runSource: RunSourceComponent;
  @ViewChild('runDownloadComponent', {static: false}) runDownload: RunDownloadComponent;

  workspace: Workspace;
  configSelected: Config;
  credentialsSelected: Credentials;

  index: Index;
  sourceItems: SourceItems;
  running = false;
  canRun = true;

  constructor(
    private app: AppService,
  ) {}

  ngOnInit() {
    this.workspace = this.app.workspace;

    if (this.configs.length > 0) {
      this.configSelected = this.configs[0];
      this.onChangeConfig(this.configSelected);
    }

    if (this.credentials.length > 0) {
      this.credentialsSelected = this.credentials[0];
    }
  }

  canLaunchAnalyse(): boolean {
    return this.canRun && !this.running;
  }

  canLaunchRun(): boolean {
    return this.canRun && !this.running;
  }

  onChangeConfig(newConfig: Config): void {
    this.app.itemsService.loadIndex(newConfig.paths.items).subscribe(itemsIndex => {
      this.index = itemsIndex;
    }, (error) => {
      console.error('Index file probably does not exists. Generating a new one. Error', error);
      this.index = this.app.sourcesService.constructIndex(this.configSelected);
    });
  }

  onAnalyseStarted(): void {
    this.running = true;
  }

  onAnalyseFinished(sourceItems: SourceItems): void {
    this.running = false;
    this.sourceItems = sourceItems;
  }

  onDownloadStarted(): void {
    this.running = true;
  }

  onMediaDownloaded(downloadMedia: DownloadMedia): void {
    this.app.downloadService.saveDownloadMedia(downloadMedia).subscribe();
  }

  onItemDownloaded(downloadItem: DownloadItem): void {
    this.app.downloadService.saveDownloadItem(downloadItem).subscribe();
  }

  onDownloadFinished(downloadItems: DownloadItems): void {
    this.running = false;
    this.app.itemsService.saveIndex(this.index).subscribe();
    this.runSource.removeDownloadedItems();
  }

  get configs(): Config[] {
    return this.workspace.configs;
  }

  get credentials(): Credentials[] {
    return this.workspace.credentials;
  }
}
