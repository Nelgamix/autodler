import {Step} from '../Step';
import {AppService} from '../../../../../providers/app.service';
import {StepItem} from '../item/StepItem';
import {Observable, of, Subject} from 'rxjs';
import {Context} from '../../Context';
import {StepModuleDefinition} from './definition/StepModuleDefinition';

export abstract class StepModule<M, I, O extends StepItem> implements Step<O> {
  public definition: StepModuleDefinition<M, I>;

  protected readonly itemFolder: string;

  protected input: I;
  protected output: O;

  constructor(protected app: AppService, protected context: Context, input: I) {
    this.input = input;
  }

  public createOutput(): O {
    if (this.output) {
      return this.output;
    }

    this.output = this.createStepItem() as O;
    this.output.run = this.createRun();
    return this.output;
  }

  // protected abstract createStepItem(): O;

  protected createStepItem(): StepItem {
    return {
      run: null,
      updates: new Subject<void>(),
      progress: 0,
    };
  }

  protected createRun(): Observable<void> {
    return of(null);
  }

  protected getItemPath(id?: string): string {
    return this.context.configuration.folder + '/' + this.itemFolder + (id ? '/' + id + '.json' : '');
  }
}
