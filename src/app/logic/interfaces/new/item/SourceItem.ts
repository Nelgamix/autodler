import {Item} from './Item';

export interface SourceItem extends Item {
  title: string;
  child: string[];
}
