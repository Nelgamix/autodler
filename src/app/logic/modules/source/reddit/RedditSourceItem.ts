import {SourceItem} from '../../../interfaces/new/item/SourceItem';

export interface RedditSourceItem extends SourceItem {
  metadata: {
    author: string;
    created: number;
    name: string;
    permalink: string;
    score: number;
    num_comments: number;
    subreddit: {
      id: string;
      name: string;
      subscribers: number;
    };
    over_18: boolean;
  };
}
