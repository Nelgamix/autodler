export interface RedditSourceCredentials {
  userAgent: string;
  clientId: string;
  clientSecret: string;
  refreshToken?: string;
  username?: string;
  password?: string;
}
