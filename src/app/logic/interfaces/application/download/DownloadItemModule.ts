import {Observable, Subject, Subscriber} from 'rxjs';
import {Item} from '../../generic/Item';
import {SourceItems} from '../source/SourceItems';
import {DownloadItem, DownloadItemAnalyseState, DownloadItemRunState} from './DownloadItems';
import {removeGetParameters} from '../../../../utils/url';
import {getExtension} from '../../../../utils/path';
import {DownloadItemModuleDefinition} from './DownloadItemModuleDefinition';
import {Media} from '../../generic/Media';
import {DownloadMedia, DownloadMediaRunState} from './DownloadMedia';

export abstract class DownloadItemModule {
  public definition: DownloadItemModuleDefinition;
  public downloadItem: DownloadItem;

  protected observable: Observable<DownloadItemAnalyseState>;
  protected subscriber: Subscriber<DownloadItemAnalyseState>;

  protected constructor(
    protected item: Item,
    protected sourceItems: SourceItems,
  ) {
    this.downloadItem = this.constructDownloadItem();
  }

  public abstract run(): Observable<DownloadItemAnalyseState>;

  public addDownloadMedia(downloadMedia: DownloadMedia): void {
    this.downloadItem.output.push(downloadMedia);
    this.downloadItem.input.medias.push(downloadMedia.output);
  }

  public updateProgress(): void {
    if (this.downloadItem.run.state === DownloadItemRunState.Waiting) {
      this.downloadItem.run.state = DownloadItemRunState.Running;
    }

    const totalWeight = this.downloadItem.output.length;
    const totalProgress = this.downloadItem.output.reduce((pv, cv) => pv + cv.run.progress, 0);
    this.downloadItem.run.progress = totalProgress / totalWeight;

    if (this.downloadItem.run.state === DownloadItemRunState.Running && this.isFinished()) {
      this.downloadItem.input.info.download.date = Date.now();
      this.downloadItem.run.state = DownloadItemRunState.Finished;
      this.downloadItem.run.progress = 100;
    }
  }

  public notifyObs(downloadMedia: DownloadMedia): void {
    this.downloadItem.run.updates.next(downloadMedia);
  }

  protected constructDownloadItem(): DownloadItem {
    return {
      input: this.item,
      output: [],
      config: this.sourceItems.config,
      modules: {
        source: this.sourceItems.module,
        downloadItem: this,
        downloadMedia: null,
      },
      analyse: {
        start: null,
        state: DownloadItemAnalyseState.Waiting,
        progress: 0,
      },
      run: {
        updates: new Subject<DownloadMedia>(),
        state: DownloadItemRunState.Waiting,
        progress: 0,
      },
    };
  }

  protected constructMedia(opts: {
    url?: string,
    mediaUrl?: string,
    extension?: string,
    uniqueFolderNumber?: number,
  } = {}): Media {
    // Get path from source (may be modified by the config)
    let path = this.downloadItem.modules.source.getPathForMedia(this.item);
    // Make a seperate folder if the number is given (multi-media items)
    if (opts.uniqueFolderNumber !== undefined) {
      path += '/' + this.item.id + '/' + opts.uniqueFolderNumber;
    } else {
      path += '/' + this.item.id;
    }
    // Return the final media with opts
    return {
      path: path + (opts.extension ? '.' + opts.extension : ''),
      type: this.definition.type,
      extension: opts.extension || '',
      info: {
        analyse: {
          done: false,
        },
        download: {
          done: false,
        },
      },
      urls: {
        page: opts.url || opts.mediaUrl || this.item.url || '',
        media: opts.mediaUrl || this.item.url || '',
      },
    };
  }

  protected initRun(): void {
    this.downloadItem.analyse.start = this.run();
  }

  protected isFinished(): boolean {
    return !Boolean(this.downloadItem.output.find(item => item.run.state !== DownloadMediaRunState.Finished));
  }

  protected basicRun(): Observable<DownloadItemAnalyseState> {
    if (this.observable) {
      return this.observable;
    }

    this.observable = new Observable<DownloadItemAnalyseState>(subscriber => {
      this.subscriber = subscriber;

      const urlClean = removeGetParameters(this.item.url);
      const extension = getExtension(urlClean);
      const media = this.constructDownloadMedia(
        this.constructMedia({
          url: this.item.url,
          mediaUrl: urlClean,
          extension,
        })
      );

      this.initRun(media);

      subscriber.next(DownloadItemAnalyseState.Finished);
      subscriber.complete();
    });

    return this.observable;
  }
}
