import {Context} from '../../../Context';
import {StepModulePriority} from '../StepModulePriority';
import {AppService} from '../../../../../../providers/app.service';

export interface StepModuleDefinition<M, I> {
  name?: string;
  description?: string;
  type: string;
  priority: StepModulePriority;
  accepts?: (input: I) => boolean;
  instantiate: (app: AppService, context: Context, input: I) => M;
}
