import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoadGuard} from './guards/load.guard';
// Pages
import {PageConfigWorkspaceComponent} from './pages/page-config-workspace/page-config-workspace.component';
import {PageConfigConfigsComponent} from './pages/page-config-configs/page-config-configs.component';
import {PageConfigCredentialsComponent} from './pages/page-config-credentials/page-config-credentials.component';
import {PageViewComponent} from './pages/page-view/page-view.component';
import {PageRunComponent} from './pages/page-run/page-run.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/config-workspace'},
  {
    path: '',
    canActivate: [LoadGuard],
    children: [
      {path: 'config-workspace', component: PageConfigWorkspaceComponent},
      {path: 'config-configs', component: PageConfigConfigsComponent},
      {path: 'config-credentials', component: PageConfigCredentialsComponent},
      {path: 'view', component: PageViewComponent},
      {path: 'run', component: PageRunComponent},
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
