import {Component, OnInit, ViewChild} from '@angular/core';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';
import {assign} from 'lodash';
import {AbstractConfig} from '../../components/sources/AbstractConfig';
import {ModalAddConfigComponent} from './modal-add-config/modal-add-config.component';
import {AppService} from '../../providers/app.service';
import {Workspace} from '../../logic/interfaces/generic/Workspace';
import {Config} from '../../logic/interfaces/generic/Config';

@Component({
  selector: 'app-page-config-configs',
  templateUrl: './page-config-configs.component.html',
  styleUrls: ['./page-config-configs.component.scss']
})
export class PageConfigConfigsComponent implements OnInit {
  @ViewChild('configurator', {static: false}) configurator: AbstractConfig;

  workspace: Workspace;

  configSelected: Config;
  formValid = false;

  constructor(
    private app: AppService,
    private modalService: NzModalService,
    private message: NzMessageService,
  ) { }

  ngOnInit() {
    this.workspace = this.app.workspace;

    // Select by default
    if (this.configs.length > 0) {
      this.selectConfig(this.configs[0]);
    }
  }

  selectConfig(config: Config): void {
    this.configSelected = config;
  }

  showModal(config?: Config): void {
    const modal = this.modalService.create({
      nzTitle: 'Add config',
      nzContent: ModalAddConfigComponent,
      nzComponentParams: {
        config,
      },
    });

    modal.afterClose.subscribe((result: Config | undefined) => {
      if (result) {
        if (this.workspace.configs.find(c => result === c)) {
          // Config exists
          this.saveConfig();
        } else {
          this.workspace.configs.push(result);
        }
      }
    });
  }

  saveConfig(): void {
    this.formValid = false;
    assign(this.configSelected, this.configurator.getConfig());
    this.app.workspaceService.saveWorkspace(this.workspace).subscribe(() => {
      this.message.success('Your config has been saved successfully.');
    });
  }

  deleteConfig(config: Config): void {
    const idx = this.workspace.configs.findIndex(c => c === config);
    if (idx >= 0) {
      this.workspace.configs.splice(idx, 1);
    }

    if (this.configs.length > 0) {
      this.selectConfig(this.configs[this.configs.length - 1]);
    } else {
      this.configSelected = undefined;
    }
  }

  validityChanges(newValidity: boolean): void {
    this.formValid = newValidity;
  }

  get configs(): Config[] {
    return this.workspace.configs;
  }
}
