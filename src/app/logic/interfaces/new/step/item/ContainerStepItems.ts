import {StepItem} from './StepItem';
import {ContainerItem} from '../../item/ContainerItem';

export interface ContainerStepItems extends StepItem {
  items: ContainerItem[];
}
