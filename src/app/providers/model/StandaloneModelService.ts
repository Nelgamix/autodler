import {Observable, of} from 'rxjs';
import {remove} from 'lodash';
import {map, tap} from 'rxjs/operators';
import {ElectronService} from '../electron.service';
import {StandaloneModel} from '../../logic/interfaces/new/StandaloneModel';

export abstract class StandaloneModelService<T extends StandaloneModel> {
  protected models: T[];

  constructor(protected electron: ElectronService) {
    this.models = [];
  }

  public getByPath(path: string): Observable<T | undefined> {
    const model = this.findByPath(path);
    return model ? of(model) : this.loadByPath(path);
  }

  public getById(basePath: string, id: string): Observable<T | undefined> {
    const model = this.findById(id);
    return model ? of(model) : this.loadById(basePath, id);
  }

  public loadByPath(path: string): Observable<T> {
    return this.electron.readFile(path).pipe(
      map((file) => JSON.parse(file)),
      tap((model) => this.add(model)),
    );
  }

  public loadById(basePath: string, id: string): Observable<T> {
    return this.electron.readFile(this.pathFromId(basePath, id)).pipe(
      map((file) => JSON.parse(file)),
      tap((model) => this.add(model)),
    );
  }

  public findByPath(path: string): T | undefined {
    return this.models.find(m => m.path === path);
  }

  public findById(id: string): T | undefined {
    return this.models.find(m => m.id === id);
  }

  public add(model: T): void {
    if (this.findById(model.id)) {
      return;
    }

    this.models.push(model);
  }

  public remove(model: T): void {
    if (!this.findById(model.id)) {
      return;
    }

    remove(this.models, m => m.id === model.id);
  }

  public save(model: T): Observable<void> {
    if (!model || !model.path) {
      return of();
    }

    return this.electron.writeFile(model.path, JSON.stringify(model, null, 4));
  }

  public delete(model: T): Observable<void> {
    if (!model || !model.path) {
      return of();
    }

    return this.electron.deleteFile(model.path).pipe(
      tap(() => this.remove(model)),
    );
  }

  public pathFromId(basePath: string, id: string): string {
    return basePath + '/' + id + '.json';
  }
}
