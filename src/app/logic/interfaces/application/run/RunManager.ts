import {RunConfig} from '../RunConfig';
import {Observable, Subject} from 'rxjs';
import {Run, RunStepModuleDefinition, RunStepProgress, RunStepState, RunStepType} from './Run';
import {RunModuleManager} from './RunModuleManager';
import {RunTasks} from './RunTasks';
import {AppService} from '../../../../providers/app.service';
import {SourceModule} from './SourceModule';

export class RunManager {
  protected sourceModules: RunStepModuleDefinition[] = [];
  protected analyseModules: RunStepModuleDefinition[] = [];
  protected downloadModules: RunStepModuleDefinition[] = [];

  protected sourceModuleManager: RunModuleManager;
  protected analyseModuleManager: RunModuleManager;
  protected downloadModuleManager: RunModuleManager;

  constructor(private app: AppService) {
    this.sourceModuleManager = new RunModuleManager(this.sourceModules);
    this.analyseModuleManager = new RunModuleManager(this.analyseModules);
    this.downloadModuleManager = new RunModuleManager(this.downloadModules);
  }

  public createRun(config: RunConfig): Run {
    return {
      configs: config,
      helpers: {
        loadItem: (id: string) => this.app.itemsService.loadItem(config.index.folder, id),
      },
      progress: {
        step: RunStepType.Source,
        state: RunStepState.Waiting,
        percentage: 0,
        started: null,
        finished: null,
        update: new Subject<void>(),
      },
      steps: {
        source: null,
        analyse: null,
        download: null,
      },
    };
  }

  private createProgress(): RunStepProgress {
    return {
      state: RunStepState.Waiting,
      percentage: 0,
      started: null,
      finished: null,
      update: new Subject<void>(),
    };
  }

  runSource(run: Run): Observable<void> {
    const moduleDefinition = this.sourceModuleManager.chooseModule(run);
    const module = moduleDefinition.instantiate(run) as SourceModule;
    const tasks = [module.run()];
    run.steps.source = {
      module,
      output: [],
      progress: this.createProgress(),
    };
    return new RunTasks(tasks).run();
  }
}
