import {SourceItem} from '../../item/SourceItem';
import {StepItem} from './StepItem';

export interface SourceStepItems extends StepItem {
  items: SourceItem[];
}
