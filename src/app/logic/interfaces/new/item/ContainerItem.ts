import {Item} from './Item';

export interface ContainerItem extends Item {
  parent: string;
  child: string[];
}
