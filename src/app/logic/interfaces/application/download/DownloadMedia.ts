import {Media} from '../../generic/Media';
import {Observable} from 'rxjs';

export enum DownloadMediaRunState {
  Waiting,
  Running,
  Finished,
}

export interface DownloadMedia {
  output: Media;
  run: {
    start: Observable<DownloadMediaRunState>;
    state: DownloadMediaRunState;
    progress: number;
    data: any;
  };
}
