import {Injectable, NgZone} from '@angular/core';
import {ipcRenderer, webFrame, remote, shell, dialog} from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import {concat, Observable, of, Subscriber} from 'rxjs';
import {getFolder} from '../utils/path';
import {last, mapTo, switchMap} from 'rxjs/operators';

@Injectable()
export class ElectronService {
  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  fs: typeof fs;
  shell: typeof shell;
  dialog: typeof dialog;

  constructor(private ngZone: NgZone) {
    // Conditional imports
    if (this.isElectron()) {
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;
      this.remote = window.require('electron').remote;
      this.shell = window.require('electron').shell;
      this.dialog = this.remote.dialog;

      this.childProcess = window.require('child_process');
      this.fs = window.require('fs');
    }
  }

  isElectron(): boolean {
    return window && window.process && window.process.type;
  }

  existsFile(path: string): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      this.fs.stat(path, (err, stats) => {
        if (err) {
          return this.subscriberCompleteInZone(subscriber, false);
        }

        this.subscriberCompleteInZone(subscriber, true);
      });
    });
  }

  readFile(path: string, encoding: string = 'utf-8'): Observable<string> {
    if (!path) {
      return of();
    }

    this.log('Read file', path);

    return new Observable<string>(subscriber => {
      this.fs.readFile(path, {encoding}, (err, file) => {
        if (err) {
          return this.subscriberErrorInZone(subscriber, err, 'File can not be read');
        }

        this.subscriberCompleteInZone(subscriber, file);
      });
    });
  }

  writeFile(path: string, data: any, options: {
    encoding?: string;
    createDirs?: boolean;
  } = {}): Observable<void> {
    if (!path || !data) {
      return of();
    }

    this.log('Write file', path);

    const dir = getFolder(path);

    return concat(
      this.existsFile(dir).pipe(switchMap((exists: boolean) => exists ? of() : this.createDirectory(dir))),
      new Observable<void>(subscriber => {
        this.fs.writeFile(path, data, {encoding: options.encoding || 'utf-8'}, err => {
          if (err) {
            return this.subscriberErrorInZone(subscriber, err, 'File can not be written');
          }

          this.subscriberCompleteInZone(subscriber);
        });
      })
    ).pipe(
      last(),
      mapTo(null)
    );
  }

  deleteFile(path: string): Observable<void> {
    if (!path) {
      return of();
    }

    this.log('Delete file', path);

    return new Observable<void>(subscriber => {
      this.fs.unlink(path, err => {
        if (err) {
          return this.subscriberErrorInZone(subscriber, err, 'File can not be deleted');
        }

        this.subscriberCompleteInZone(subscriber);
      });
    });
  }

  createDirectory(path: string): Observable<void> {
    if (!path) {
      return of();
    }

    this.log('Create directory', path);

    return new Observable<void>(subscriber => {
      this.fs.mkdir(path, {recursive: true}, err => {
        if (err) {
          return this.subscriberErrorInZone(subscriber, err, 'Directory can not be created');
        }

        this.subscriberCompleteInZone(subscriber);
      });
    });
  }

  private subscriberCompleteInZone<T>(subscriber: Subscriber<T>, data?: T): void {
    this.ngZone.run(() => {
      subscriber.next(data);
      subscriber.complete();
    });
  }

  private subscriberErrorInZone<T>(subscriber: Subscriber<T>, error?: any, printMessage?: string): void {
    if (printMessage) {
      this.error(printMessage, error);
    }

    this.ngZone.run(() => {
      subscriber.error(error);
      subscriber.complete();
    });
  }

  private log(...messages: any): void {
    console.log('[Electron]', ...messages);
  }

  private error(...messages: any): void {
    console.error('[Electron]', ...messages);
  }
}
