import {DownloadMediaModule} from './DownloadMediaModule';
import {DownloadModuleWeight} from './DownloadModuleWeight';

export interface DownloadMediaModuleDefinition {
  name: string;
  description: string;
  type: string;
  weight: DownloadModuleWeight;
  instantiate: () => DownloadMediaModule;
  matches: () => boolean;
}
