import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AbstractConfig} from '../../AbstractConfig';
import {RedditSourceConfiguration} from '../../../../logic/modules/source/reddit/RedditConfig';
import {RedditSourcePlaylist} from '../../../../logic/modules/source/reddit/RedditPlaylist';
import {assign} from 'lodash';

@Component({
  selector: 'app-reddit-config',
  templateUrl: './reddit-config.component.html',
  styleUrls: ['./reddit-config.component.scss']
})
export class RedditConfigComponent implements OnInit, OnChanges, AbstractConfig {
  @Input() config: RedditSourceConfiguration;
  @Output() validityChanges = new EventEmitter<boolean>();

  playlistOptions = Object.entries(RedditSourcePlaylist);
  form = this.fb.group({
    playlist: ['', [Validators.required]],
    nsfwFolder: [''],
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.checkConfig();
    this.configToForm();

    this.form.valueChanges.subscribe(() => {
      this.validityChanges.emit(this.form.valid);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.checkConfig();
    this.configToForm();
  }

  getConfig(): RedditSourceConfiguration {
    if (!this.form.valid) {
      return;
    }

    this.formToConfig();

    return this.config;
  }

  resetConfig(): void {
  }

  setConfig(config: RedditSourceConfiguration): void {
    this.config = config;
    this.configToForm();
  }

  formToConfig(): void {
    assign(this.config, this.form.getRawValue());
  }

  configToForm(): void {
    this.form.patchValue(this.config);
  }

  checkConfig(): void {
    this.config.type = 'reddit';
    this.config.playlist = this.config.playlist || RedditSourcePlaylist.Liked;
    this.config.nsfwFolder = this.config.nsfwFolder || '';
  }
}
