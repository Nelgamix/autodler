import {Runner, RunnerSettings} from './Runner';
import {ProviderStepModule} from '../step/module/ProviderStepModule';
import {ContainerItem} from '../item/ContainerItem';
import {AppService} from '../../../../providers/app.service';
import {ProviderStepItems} from '../step/item/ProviderStepItems';
import {Context} from '../Context';
import {StepModuleDefinition} from '../step/module/definition/StepModuleDefinition';

const modules: StepModuleDefinition<ProviderStepModule<any>, ContainerItem>[] = [];

export class ProviderRunner extends Runner<ContainerItem, ProviderStepItems> {
  constructor(app: AppService, context: Context, settings: RunnerSettings) {
    super(modules, app, context, settings);
  }
}
