import {Media} from '../../generic/Media';
import {Item} from '../../generic/Item';
import {Observable, Subscriber} from 'rxjs';
import {DownloadError} from '../error/DownloadError';
import {DownloadItemModule} from './DownloadItemModule';
import Axios from 'axios';
import {DownloadMedia, DownloadMediaRunState} from './DownloadMedia';
import {DownloadMediaModuleDefinition} from './DownloadMediaModuleDefinition';

export abstract class DownloadMediaModule {
  public definition: DownloadMediaModuleDefinition;
  public downloadMedia: DownloadMedia;

  protected observable: Observable<DownloadMediaRunState>;
  protected subscriber: Subscriber<DownloadMediaRunState>;

  constructor(
    protected item: Item,
    protected media: Media,
    protected downloadItemModule: DownloadItemModule,
  ) {
    this.downloadMedia = this.constructDownloadMedia();
  }

  public abstract run(): Observable<DownloadMediaRunState>;

  public isFinished(): boolean {
    return this.downloadMedia.run.state === DownloadMediaRunState.Finished;
  }

  protected constructDownloadMedia(): DownloadMedia {
    return {
      output: this.media,
      run: {
        start: null,
        state: DownloadMediaRunState.Waiting,
        progress: 0,
        data: null,
      },
    };
  }

  protected initRun(): void {
    this.downloadMedia.run.start = this.run();
    this.downloadItemModule.addDownloadMedia(this.downloadMedia);
  }

  protected updateProgress(progress?: number): void {
    if (this.downloadMedia.run.state ===  DownloadMediaRunState.Waiting) {
      this.downloadMedia.run.state = DownloadMediaRunState.Running;
    }

    if (progress !== undefined) {
      this.downloadMedia.run.progress = progress;
    }

    if (this.downloadMedia.run.progress >= 100 && this.downloadMedia.run.data) {
      this.downloadMedia.run.progress = 100;
      this.downloadMedia.run.state = DownloadMediaRunState.Finished;
      this.downloadMedia.output.info.download.done = true;
      this.downloadMedia.output.info.download.date = Date.now();
    }

    this.downloadItemModule.updateProgress();
    this.notifyObs();
  }

  protected setError(error: DownloadError): void {
    this.downloadMedia.output.info.download.error = error;
    this.downloadMedia.run.state = DownloadMediaRunState.Finished;
    this.downloadMedia.run.progress = 100;
    this.downloadMedia.run.data = null;

    this.updateProgress();
  }

  protected setData(data: any): void {
    this.downloadMedia.run.data = data;
    this.downloadMedia.output.size = data.length;

    this.updateProgress();
  }

  protected notifyObs(): void {
    this.subscriber.next(this.downloadMedia.run.state);

    if (this.downloadMedia.run.state === DownloadMediaRunState.Finished) {
      this.subscriber.complete();
    }

    this.downloadItemModule.notifyObs(this.downloadMedia);
  }

  protected basicRun(): Observable<DownloadMediaRunState> {
    if (this.observable) {
      return this.observable;
    }

    this.observable = new Observable<DownloadMediaRunState>(subscriber => {
      this.subscriber = subscriber;

      Axios.get(this.downloadMedia.output.urls.media, {
        responseType: 'arraybuffer',
        onDownloadProgress: (progress: ProgressEvent) => {
          this.updateProgress((progress.loaded / progress.total) * 100);
        },
      }).then(res => {
        this.setData(Buffer.from(res.data, 'binary'));
        this.updateProgress(100);
      }).catch((err) => {
        console.error(err);
        this.setError(DownloadError.Unknown);
      });
    });

    return this.observable;
  }
}
