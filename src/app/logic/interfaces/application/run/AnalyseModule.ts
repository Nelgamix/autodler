import {RunStepAnalyseModule, RunStepDownload} from './Run';
import {Observable} from 'rxjs';

export abstract class AnalyseModule implements RunStepAnalyseModule {
  abstract addStepDownload(step: RunStepDownload): void;
  abstract run(): Observable<void>;
}
