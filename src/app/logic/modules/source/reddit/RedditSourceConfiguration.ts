import {RedditSourceCredentials} from './RedditSourceCredentials';
import {RedditSourceTarget} from './RedditSourceTarget';
import {SourceConfiguration} from '../../../interfaces/new/configuration/SourceConfiguration';

export interface RedditSourceConfiguration extends SourceConfiguration {
  target: RedditSourceTarget;
  credentials: RedditSourceCredentials;
}
