import {RunStepDownloadModule} from './Run';
import {Observable} from 'rxjs';

export abstract class DownloadModule implements RunStepDownloadModule {
  abstract run(): Observable<void>;
}
