import {Run, RunStepAnalyse, RunStepModuleDefinition, RunStepSourceModule} from './Run';
import {Observable} from 'rxjs';
import {Index} from '../../generic';
import {Item} from '../../generic/Item';

export abstract class SourceModule implements RunStepSourceModule {
  abstract definition: RunStepModuleDefinition;

  static createIndex(path: string): Index {
    return {
      folder: path,
      items: {
        success: [],
        errors: [],
        unmarked: [],
      },
    };
  }

  constructor(protected object: Run) {
  }

  abstract addStepAnalyse(step: RunStepAnalyse): void;
  abstract run(): Observable<void>;

  protected constructItem(id?: string, title?: string, url?: string): Item {
    return {
      path: this.object.configs.config.paths.items + (id ? '/' + id + '.json' : ''),
      id: id || '',
      type: this.definition.type,
      created: Date.now(),
      title: title || '',
      url: url,
      medias: [],
      info: {
        marked: true,
        analyse: {
          done: false,
        },
      },
    };
  }
}
