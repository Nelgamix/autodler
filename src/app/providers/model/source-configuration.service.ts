import { Injectable } from '@angular/core';
import {ElectronService} from '../electron.service';
import {StandaloneModelService} from './StandaloneModelService';
import {SourceConfiguration} from '../../logic/interfaces/new/SourceConfiguration';

@Injectable({
  providedIn: 'root'
})
export class SourceConfigurationService extends StandaloneModelService<SourceConfiguration> {
  constructor(electron: ElectronService) {
    super(electron);
  }
}
