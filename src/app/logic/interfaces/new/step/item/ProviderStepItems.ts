import {StepItem} from './StepItem';
import {ProviderItem} from '../../item/ProviderItem';

export interface ProviderStepItem extends StepItem {
  item: ProviderItem;
  data: any;
}

export interface ProviderStepItems extends StepItem {
  items: ProviderStepItem[];
}
