import {StandaloneModel} from './StandaloneModel';

export interface IndexStepData {
  lastRun?: number;
  items: string[];
}

export interface Index extends StandaloneModel {
  source: IndexStepData;
  container: IndexStepData;
  provider: IndexStepData;
}
