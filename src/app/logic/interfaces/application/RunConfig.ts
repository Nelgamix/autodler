import {Config} from '../generic/Config';
import {Credentials} from '../generic/Credentials';
import {Index} from '../generic';

export interface RunConfig {
  credentials: Credentials;
  config: Config;
  index: Index;
}
