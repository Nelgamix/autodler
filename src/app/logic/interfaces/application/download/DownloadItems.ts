import {Item} from '../../generic/Item';
import {RunConfig} from '../RunConfig';
import {SourceModule} from '../source/SourceModule';
import {Observable, Subject} from 'rxjs';
import {DownloadMedia} from './DownloadMedia';
import {DownloadMediaModule} from './DownloadMediaModule';
import {DownloadItemModule} from './DownloadItemModule';

export enum DownloadItemAnalyseState {
  Waiting,
  Analysing,
  Finished,
}

export enum DownloadItemRunState {
  Waiting,
  Running,
  Finished,
}

export enum DownloadItemsRunState {
  Waiting,
  Running,
  Finished,
}

export interface DownloadItem {
  input: Item;
  output: DownloadMedia[];
  // Other
  config: RunConfig;
  modules: {
    source: SourceModule;
    downloadItem: DownloadItemModule;
    downloadMedia: DownloadMediaModule;
  };
  analyse: {
    start: Observable<DownloadItemAnalyseState>;
    state: DownloadItemAnalyseState;
    progress: number;
  };
  run: {
    updates: Subject<DownloadMedia>;
    state: DownloadItemRunState;
    progress: number;
  };
}

export interface DownloadItems {
  output: DownloadItem[];
  run: {
    state: DownloadItemsRunState;
    progress: number;
  };
}

export function getDownloadItemsNotMarked(downloadItems: DownloadItems): DownloadItem[] {
  return downloadItems.output.filter(item => !item.input.info.marked);
}

export function getDownloadItemsMarked(downloadItems: DownloadItems): DownloadItem[] {
  return downloadItems.output.filter(item => item.input.info.marked);
}
