import {Step} from './Step';
import {ProviderStepItems} from './item/ProviderStepItems';

export interface ProviderStep extends Step<ProviderStepItems> {
}
