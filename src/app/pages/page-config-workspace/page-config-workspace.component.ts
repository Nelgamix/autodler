import {Component, OnInit} from '@angular/core';
import {NzModalService} from 'ng-zorro-antd';
import {ModalAddWorkspaceComponent} from './modal-add-workspace/modal-add-workspace.component';
import {AppService} from '../../providers/app.service';
import {Workspace} from '../../logic/interfaces/generic/Workspace';

@Component({
  selector: 'app-page-config-workspace',
  templateUrl: './page-config-workspace.component.html',
  styleUrls: ['./page-config-workspace.component.scss']
})
export class PageConfigWorkspaceComponent implements OnInit {
  workspaces: string[];
  workspace: Workspace;

  constructor(
    private app: AppService,
    private modalService: NzModalService,
  ) { }

  ngOnInit() {
    this.workspaces = this.app.openWorkspaces;
    this.workspace = this.app.workspace;
  }

  openAdd(): void {
    this.openEdit();
  }

  openEdit(): void {
    this.openModal(this.workspace);
  }

  openModal(workspace?: Workspace): void {
    const modal = this.modalService.create({
      nzTitle: 'Add workspace',
      nzContent: ModalAddWorkspaceComponent,
      nzComponentParams: {
        workspace,
      },
    });

    modal.afterClose.subscribe((result) => {
      if (result) {
        this.changeWorkspace(result);
      }
    });
  }

  deleteWorkspace(): void {
    this.app.closeWorkspace();
    this.app.workspaceService.deleteWorkspace(this.workspace).subscribe();
    this.workspace = undefined;
  }

  changeWorkspace(path: string): void {
    this.app.openWorkspace(path).subscribe((workspace) => this.workspace = workspace);
  }
}
