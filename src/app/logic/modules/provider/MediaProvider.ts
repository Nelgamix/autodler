import {ProviderStepModule} from '../../interfaces/new/step/module/ProviderStepModule';
import {ContainerItem} from '../../interfaces/new/item/ContainerItem';
import {AppService} from '../../../providers/app.service';
import {StepModulePriority} from '../../interfaces/new/step/module/StepModulePriority';
import {ProviderStepModuleDefinition} from '../../interfaces/new/step/module/definition/ProviderStepModuleDefinition';
import {Context} from '../../interfaces/new/Context';

export class MediaProvider extends ProviderStepModule<MediaProvider> {
  public static matcher: RegExp = /^.+\.(jpg|jpeg|png|mp4|mkv|webm|flv)(\?.*)?$/;

  public static definition: ProviderStepModuleDefinition<MediaProvider> = {
    name: 'Media',
    description: 'Simple Media provider module.',
    type: 'media',
    priority: StepModulePriority.Minimal,
    accepts: (input: ContainerItem) => MediaProvider.matcher.test(input.url),
    instantiate: (app: AppService, context: Context, input: ContainerItem) => new MediaProvider(app, context, input),
  };

  public definition = MediaProvider.definition;

  /*public static definition: DownloadItemModuleDefinition = {
    name: 'Media',
    type: 'media',
    description: 'Downloads basic media.',
    weight: DownloadModuleWeight.Minimal,
    instantiate: (item: Item, input: SourceItems) => new MediaDownload(item, input),
    matches: (item: Item, input: SourceItems) => matcher.test(item.url),
  };

  protected definition: DownloadItemModuleDefinition = MediaDownload.definition;

  protected constructor(item: Item, input: SourceItems) {
    super(item, input);
    this.initAnalyse();
  }

  analyse(): Observable<DownloadItemAnalyseState> {
    return this.basicAnalyse();
  }

  run(downloadMedia: DownloadMedia): Observable<DownloadMediaRunState> {
    return this.basicRun(downloadMedia);
  }*/
}
