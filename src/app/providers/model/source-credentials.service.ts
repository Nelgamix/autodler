import { Injectable } from '@angular/core';
import {StandaloneModelService} from './StandaloneModelService';
import {SourceCredentials} from '../../logic/interfaces/new/SourceCredentials';
import {ElectronService} from '../electron.service';

@Injectable({
  providedIn: 'root'
})
export class SourceCredentialsService extends StandaloneModelService<SourceCredentials> {
  constructor(electron: ElectronService) {
    super(electron);
  }
}
