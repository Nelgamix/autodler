import {Index} from '../../../interfaces/new';

export interface RedditIndex extends Index {
  lastItem?: string;
}
