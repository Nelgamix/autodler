import {Injectable} from '@angular/core';
import {StandaloneModelService} from './StandaloneModelService';
import {Workspace} from '../../logic/interfaces/new/Workspace';
import {ElectronService} from '../electron.service';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceService extends StandaloneModelService<Workspace> {
  constructor(electron: ElectronService) {
    super(electron);
  }
}
