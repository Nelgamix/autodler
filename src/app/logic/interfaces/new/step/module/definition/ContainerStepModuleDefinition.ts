import {StepModuleDefinition} from './StepModuleDefinition';
import {SourceItem} from '../../../item/SourceItem';

export interface ContainerStepModuleDefinition<T> extends StepModuleDefinition<T, SourceItem> {
}
