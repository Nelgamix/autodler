import {StandaloneModel} from '../StandaloneModel';

export enum ItemError {
  Unknown,
  NoModule,
  NetworkUnavailable,
}

export enum ItemStatus {
  Standby,
  Running,
  Finished,
}

export interface ItemInformation {
  created: number;
  finished?: number;
  marked: boolean;
  error?: {
    type: ItemError;
    message: string;
  };
}

export interface Item extends StandaloneModel {
  url: string;
  status: ItemStatus;
  information: ItemInformation;
}
