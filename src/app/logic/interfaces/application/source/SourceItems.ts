import {Item} from '../../generic/Item';
import {SourceModule} from './SourceModule';
import {RunConfig} from '../RunConfig';
import {Observable} from 'rxjs';

export enum SourceItemsRunState {
  Waiting,
  Running,
  Finished,
}

export interface SourceItems {
  output: Item[];
  module: SourceModule;
  config: RunConfig;
  run: {
    start: Observable<SourceItemsRunState>;
    state: SourceItemsRunState;
    progress: number;
  };
}
