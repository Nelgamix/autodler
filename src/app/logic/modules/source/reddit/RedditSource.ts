import {SourceStepModule} from '../../../interfaces/new/step/module/SourceStepModule';
import * as Snoowrap from 'snoowrap';
import {Comment, Submission} from 'snoowrap';
import {completeObject} from '../../../../utils/util';
import {Index} from '../../../interfaces/new';
import {RedditIndex} from './RedditIndex';
import {defer, forkJoin, from, Observable, of} from 'rxjs';
import {RedditSourceCredentials} from './RedditSourceCredentials';
import {RedditSourceItem} from './RedditSourceItem';
import {SourceItem} from '../../../interfaces/new/item/SourceItem';
import {map, mapTo, switchMap, tap} from 'rxjs/operators';
import {RedditSourceConfiguration} from './RedditSourceConfiguration';
import {RedditSourcePlaylist} from './RedditSourcePlaylist';
import {AppService} from '../../../../providers/app.service';
import {StepModulePriority} from '../../../interfaces/new/step/module/StepModulePriority';
import {SourceStepModuleDefinition} from '../../../interfaces/new/step/module/definition/SourceStepModuleDefinition';
import {Context} from '../../../interfaces/new/Context';
import {SourceConfiguration} from '../../../interfaces/new/configuration/SourceConfiguration';

export class RedditSource extends SourceStepModule<RedditSource, RedditSourceConfiguration> {
  public static readonly REDDIT_URL = 'https://reddit.com';

  public static definition: SourceStepModuleDefinition<RedditSource, RedditSourceConfiguration> = {
    name: 'Reddit',
    description: 'Source for Reddit.',
    type: 'reddit',
    priority: StepModulePriority.Normal,
    accepts: (input: RedditSourceConfiguration) => input.type === RedditSource.definition.type,
    instantiate: (app: AppService, context: Context, input: RedditSourceConfiguration) => new RedditSource(app, context, input),
  };

  public definition = RedditSource.definition;

  private snoowrap: Snoowrap;

  constructor(app: AppService, context: Context, input: RedditSourceConfiguration) {
    super(app, context, input);
    this.snoowrap = new Snoowrap(input.credentials as RedditSourceCredentials);
  }

  public createIndex(): RedditIndex {
    return completeObject<Index, RedditIndex>(super.createIndex(), {});
  }

  protected createConfiguration(): RedditSourceConfiguration {
    return completeObject<SourceConfiguration, RedditSourceConfiguration>(super.createConfiguration(), {
      target: {
        playlist: RedditSourcePlaylist.Saved,
        nsfwFolder: undefined,
      },
      credentials: {
        userAgent: '',
        clientId: '',
        clientSecret: '',
        password: '',
        username: '',
        refreshToken: '',
      },
    });
  }

  protected createRun(): Observable<void> {
    switch (this.input.target.playlist) {
      case RedditSourcePlaylist.Saved: return this.runSaved();
      case RedditSourcePlaylist.Liked: return this.runUpvoted();
    }
  }

  protected createRedditSourceItem(submission: Submission): RedditSourceItem {
    return completeObject<SourceItem, RedditSourceItem>(super.createSourceItem({
      id: submission.id,
      title: submission.title,
      url: submission.url
    }), {
      metadata: {
        author: submission.author.name,
        name: submission.name,
        created: submission.created_utc,
        score: submission.score,
        num_comments: submission.num_comments,
        permalink: RedditSource.REDDIT_URL + submission.permalink,
        over_18: submission.over_18,
        subreddit: {
          id: submission.subreddit_id,
          name: submission.subreddit_name_prefixed,
          subscribers: submission.subreddit_subscribers,
        },
      }
    });
  }

  private runSaved(): Observable<void> {
    return this.runUpvotedOrSaved(
      this.getOptions().pipe(
        switchMap((opts) => from(this.snoowrap.getMe().getSavedContent(opts)))
      ),
    );
  }

  private runUpvoted(): Observable<void> {
    return this.runUpvotedOrSaved(
      this.getOptions().pipe(
        switchMap((opts) => from(this.snoowrap.getMe().getUpvotedContent(opts)))
      ),
    );
  }

  private runUpvotedOrSaved(obs: Observable<(Comment | Submission)[]>): Observable<void> {
    return obs.pipe(
      map((result: (Comment | Submission)[]) => {
        return result
          .filter(upv => upv['url'])
          .map<RedditSourceItem>((sub: Submission) => this.createRedditSourceItem(sub));
      }),
      tap((items: RedditSourceItem[]) => {
        this.output.items.push(...items);
        if (items && items.length > 0) {
          const index = this.context.index as RedditIndex;
          index.lastItem = items[items.length - 1].id;
        }
      }),
      mapTo(null),
    );
  }

  private getOptions(): Observable<any> {
    return defer(() => {
      const index = this.context.index as RedditIndex;
      const opts = {};
      const obs = [];

      if (index.lastItem) {
        const basePath = this.getItemPath();
        obs.push(this.app.sourceItemService.loadById(basePath, index.lastItem).pipe(
          tap((item: RedditSourceItem) => opts['after'] = item.metadata.name),
        ));
      }

      return obs.length > 0 ? forkJoin(...obs).pipe(mapTo(opts)) : of(opts);
    });
  }
}
