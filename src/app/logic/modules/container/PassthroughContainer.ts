import {ContainerStepModule} from '../../interfaces/new/step/module/ContainerStepModule';
import {StepModulePriority} from '../../interfaces/new/step/module/StepModulePriority';
import {MediaProvider} from '../provider/MediaProvider';
import {SourceItem} from '../../interfaces/new/item/SourceItem';
import {AppService} from '../../../providers/app.service';
import {Context} from '../../interfaces/new/Context';
import {ContainerStepModuleDefinition} from '../../interfaces/new/step/module/definition/ContainerStepModuleDefinition';

const matchers: RegExp[] = [
  MediaProvider.matcher,
];

export class PassthroughContainer extends ContainerStepModule<PassthroughContainer> {
  public static definition: ContainerStepModuleDefinition<PassthroughContainer> = {
    type: 'pass-through',
    priority: StepModulePriority.Default,
    accepts: (input: SourceItem) => Boolean(matchers.find(m => m.test(input.url))),
    instantiate: (app: AppService, context: Context, input: SourceItem) => new PassthroughContainer(app, context, input),
  };

  public definition = PassthroughContainer.definition;
}
