import {SourceModule} from './SourceModule';
import {Config} from '../../generic/Config';
import {Index} from '../../generic';
import {RunConfig} from '../RunConfig';

export interface SourceModuleDefinition {
  name: string;
  type: string;
  description: string;
  instantiate: (config: RunConfig) => SourceModule;
  constructIndex: (config: Config) => Index;
}
