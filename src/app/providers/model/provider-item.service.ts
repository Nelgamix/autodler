import { Injectable } from '@angular/core';
import {StandaloneModelService} from './StandaloneModelService';
import {ProviderItem} from '../../logic/interfaces/new/item/ProviderItem';
import {ElectronService} from '../electron.service';

@Injectable({
  providedIn: 'root'
})
export class ProviderItemService extends StandaloneModelService<ProviderItem> {
  constructor(electron: ElectronService) {
    super(electron);
  }
}
