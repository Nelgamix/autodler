import { NgModule } from '@angular/core';

// Icons
import {NZ_ICONS, NzIconModule} from 'ng-zorro-antd';
import {IconDefinition} from '@ant-design/icons-angular';
import {
  DeleteOutline,
  ImportOutline,
  KeyOutline,
  PlusOutline,
  SaveOutline,
  SettingOutline,
  ExportOutline
} from '@ant-design/icons-angular/icons';
// Icon imports
const icons: IconDefinition[] = [
  PlusOutline,
  ImportOutline,
  SettingOutline,
  KeyOutline,
  SaveOutline,
  DeleteOutline,
  ExportOutline,
];

@NgModule({
  imports: [
    NzIconModule,
  ],
  exports: [
    NzIconModule,
  ],
  providers: [
    { provide: NZ_ICONS, useValue: icons },
  ],
})
export class IconsModule { }
