import {Configuration} from './Configuration';
import {StandaloneModel} from '../StandaloneModel';

export interface SourceConfiguration extends StandaloneModel {
  configuration: Configuration;
}
