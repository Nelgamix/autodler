import {Run, RunStepModuleDefinition} from './Run';

export class RunModuleManager {
  constructor(protected modules: RunStepModuleDefinition[]) {
  }

  public chooseModule(run: Run): RunStepModuleDefinition {
    for (const module of this.modules) {
      if (module.accepts(run)) {
        return module;
      }
    }
  }
}
