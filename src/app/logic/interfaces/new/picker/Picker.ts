export abstract class Picker<T> {
  constructor(protected modules: T[]) {
  }

  abstract pick(): T | undefined;
}
