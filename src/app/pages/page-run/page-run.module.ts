import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared.module';
import { RunSourceComponent } from './run-source/run-source.component';
import { RunDownloadComponent } from './run-download/run-download.component';

@NgModule({
  declarations: [
    RunSourceComponent,
    RunDownloadComponent,
  ],
  imports: [
    SharedModule,
  ],
  exports: [
    RunSourceComponent,
    RunDownloadComponent
  ]
})
export class PageRunModule { }
