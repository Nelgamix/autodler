import {EventEmitter} from '@angular/core';
import {Config} from '../../logic/interfaces/generic/Config';

export interface AbstractConfig {
  // Config sent into input to modify
  config: Config;
  // Emitter that sends boolean describing form validity
  validityChanges: EventEmitter<boolean>;

  // Creates a new config or resets to default
  resetConfig(): void;
  // Gets the current config
  getConfig(): Config;
  // Sets the config
  setConfig(config: Config): void;
}
