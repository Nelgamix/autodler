import {Step} from './Step';
import {Index} from '../Index';
import {SourceStepItems} from './item/SourceStepItems';

export interface SourceStep extends Step<SourceStepItems> {
  createIndex(): Index;
}
