import {Step} from './Step';
import {ContainerStepItems} from './item/ContainerStepItems';

export interface ContainerStep extends Step<ContainerStepItems> {
}
