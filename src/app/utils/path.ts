const pathfs = require('path');

import {arrayLast} from './util';

/**
 * Get the extension in the path given.
 * Ex: C:/Users/tmp/file.json => json
 * @param path the path to transform
 */
export function getExtension(path: string): string | undefined {
  return path.slice((Math.max(0, path.lastIndexOf('.')) || Infinity) + 1);
}

/**
 * Get the filename (with extension) in the path given.
 * Ex: C:/Users/tmp/file.json => file.json
 * @param path the path to transform
 */
export function getFile(path: string): string | undefined {
  return arrayLast(path.split('/'));
}

/**
 * Get the filename (without extension) in the path given.
 * Ex: C:/Users/tmp/file.json => file
 * @param path the path to transform
 */
export function getFilename(path: string): string | undefined {
  const file = arrayLast(path.split('/'));
  const tokens = file.split('.');
  return tokens.splice(tokens.length - 1, 1).join('.');
}

/**
 * Get the folder in the path given.
 * Ex: C:/Users/tmp/file.json => C:/Users/tmp
 * Ex with parent = 1 => C:/Users/tmp/file.json => C:/Users
 * @param path the path to transform
 * @param parent the number of folders to go up
 */
export function getFolder(path: string, parent: number = 0): string | undefined {
  const arr = path.split('/');
  return arr.splice(0, arr.length - (1 + parent)).join('/');
}

export function sanitizePath(path: string): string {
  const rt = new RegExp('^\/+|\/+$', 'g');
  const pathSep = new RegExp(pathfs.sep === '\\' ? '\\\\' : pathfs.sep, 'g');
  const finalPath = path
    .replace(pathSep, '/')
    .replace(rt, '');
  console.log(path, pathfs.sep, finalPath);
  return finalPath;
}
