import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AppService} from '../../providers/app.service';
import {Workspace} from '../../logic/interfaces/generic/Workspace';
import {Config} from '../../logic/interfaces/generic/Config';
import {Index} from '../../logic/interfaces/generic';
import {Item} from '../../logic/interfaces/generic/Item';

@Component({
  selector: 'app-page-view',
  templateUrl: './page-view.component.html',
  styleUrls: ['./page-view.component.scss']
})
export class PageViewComponent implements OnInit, AfterViewInit {
  workspace: Workspace;

  config: Config;
  index: Index;
  items: Item[];

  selectedItem: Item;

  constructor(
    private app: AppService,
  ) { }

  ngOnInit(): void {
    this.workspace = this.app.workspace;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      // Select default
      if (this.configs.length > 0) {
        this.config = this.configs[0];
        this.onChangeConfig(this.config);
      }
    });
  }

  onChangeConfig(config: Config): void {
    this.items = [];
    this.app.itemsService.loadIndex(config.paths.items).subscribe(itemsIndex => {
      this.index = itemsIndex;

      this.app.itemsService.loadItems(this.index.folder, this.pickItems()).subscribe(item => {
        this.items.push(item);
      });
    });
  }

  selectItem(item: Item): void {
    this.selectedItem = item;
  }

  getImageForItem(item: Item): string {
    if (item.medias.length === 0) {
      return '';
    }

    return 'file:///' + item.medias[0].path;
  }

  get configs(): Config[] {
    return this.workspace ? this.workspace.configs : [];
  }

  private pickItems(max: number = Infinity): string[] {
    return this.index.items.success.slice(0, Math.min(this.index.items.success.length, max));
  }
}
