import {Observable, Subject} from 'rxjs';

export interface StepItem {
  progress: number;
  updates: Subject<void>;
  run: Observable<void>;
}
