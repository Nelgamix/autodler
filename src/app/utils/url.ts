export function removeGetParameters(url: string): string {
  return url.split('?', 1)[0];
}
