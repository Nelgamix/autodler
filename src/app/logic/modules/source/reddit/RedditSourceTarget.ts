import {RedditSourcePlaylist} from './RedditSourcePlaylist';

export interface RedditSourceTarget {
  playlist: RedditSourcePlaylist;
  nsfwFolder: string | undefined;
}
