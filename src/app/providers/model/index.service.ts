import { Injectable } from '@angular/core';
import {StandaloneModelService} from './StandaloneModelService';
import {Index} from '../../logic/interfaces/new';
import {ElectronService} from '../electron.service';

@Injectable({
  providedIn: 'root'
})
export class IndexService extends StandaloneModelService<Index> {
  constructor(electron: ElectronService) {
    super(electron);
  }
}
