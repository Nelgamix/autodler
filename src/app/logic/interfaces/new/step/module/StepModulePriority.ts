export enum StepModulePriority {
  NoModule,
  Default,
  Minimal,
  Low,
  Normal,
  High,
  Critical,
}
