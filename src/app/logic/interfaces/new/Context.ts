import {Index} from './Index';
import {Configuration} from './Configuration';

export interface Context {
  configuration: Configuration;
  index: Index;
}
