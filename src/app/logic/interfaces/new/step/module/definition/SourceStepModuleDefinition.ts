import {StepModuleDefinition} from './StepModuleDefinition';
import {SourceConfiguration} from '../../../configuration/SourceConfiguration';

export interface SourceStepModuleDefinition<T, I extends SourceConfiguration> extends StepModuleDefinition<T, I> {
}
