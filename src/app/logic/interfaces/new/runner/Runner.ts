import {StepItem} from '../step/item/StepItem';
import {remove} from 'lodash';
import {AppService} from '../../../../providers/app.service';
import {StepModuleDefinition} from '../step/module/definition/StepModuleDefinition';
import {Context} from '../Context';

export interface RunnerSettings {
  maxConcurrentTasks: number;
}

export abstract class Runner<I, O extends StepItem> {
  protected tasks: O[];
  protected tasksReady: O[];
  protected tasksRunning: O[];
  protected tasksFinished: O[];

  protected constructor(
    protected modules: StepModuleDefinition<any, I>[],
    protected app: AppService,
    protected context: Context,
    protected settings: RunnerSettings,
  ) {
  }

  protected chooseModule(input: I): StepModuleDefinition<any, I> {
    for (const module of this.modules) {
      if (module.accepts(input)) {
        return module;
      }
    }
  }

  public init(input: I): O[] {
    const tasks = this.chooseModule(input).instantiate(this.app, this.context, input);
    this.initWithTasks(tasks);
    return tasks;
  }

  public start(): void {
    this.startTasks();
  }

  protected startTasks(): void {
    while (this.tasksRunning.length < this.settings.maxConcurrentTasks && this.tasksReady.length > 0) {
      const nextTask = this.getNextTask();
      this.startTask(nextTask);
    }
  }

  protected startTask(task: O): void {
    this.setTaskRunning(task);
    task.run.subscribe(() => {
      this.setTaskFinished(task);
      this.startTasks();
    });
  }

  protected getNextTask(): O {
    return this.tasksReady[0];
  }

  protected initWithTasks(tasks: O[]): void {
    this.tasks = tasks;
    this.tasksReady = this.tasks.slice();
  }

  protected setTaskRunning(task: O): void {
    remove(this.tasksReady, t => t === task);
    this.tasksRunning.push(task);
  }

  protected setTaskFinished(task: O): void {
    remove(this.tasksRunning, t => t === task);
    this.tasksFinished.push(task);
  }
}
