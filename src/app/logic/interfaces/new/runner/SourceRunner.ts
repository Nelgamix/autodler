import {Runner, RunnerSettings} from './Runner';
import {SourceStepModule} from '../step/module/SourceStepModule';
import {AppService} from '../../../../providers/app.service';
import {RedditSource} from '../../../modules/source/reddit/RedditSource';
import {NoModuleSource} from '../../../modules/source/NoModuleSource';
import {SourceStepItems} from '../step/item/SourceStepItems';
import {SourceConfiguration} from '../configuration/SourceConfiguration';
import {StepModuleDefinition} from '../step/module/definition/StepModuleDefinition';
import {Context} from '../Context';

export class SourceRunner extends Runner<SourceConfiguration, SourceStepItems> {
  constructor(app: AppService, context: Context, settings: RunnerSettings) {
    super(modules, app, context, settings);
  }
}
