import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AbstractCredentials} from '../../AbstractCredentials';
import {assign} from 'lodash';
import {RedditSourceCredentials} from '../../../../logic/modules/source/reddit/RedditCredentials';

@Component({
  selector: 'app-reddit-credentials',
  templateUrl: './reddit-credentials.component.html',
  styleUrls: ['./reddit-credentials.component.scss']
})
export class RedditCredentialsComponent implements OnInit, OnChanges, AbstractCredentials {
  @Input() credentials: RedditSourceCredentials;
  @Output() validityChanges = new EventEmitter<boolean>();

  form = this.fb.group({
    userAgent: ['', [Validators.required]],
    clientId: ['', [Validators.required]],
    clientSecret: ['', [Validators.required]],
    refreshToken: ['', []],
    username: ['', []],
    password: ['', []],
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.checkCredentials();
    this.credentialsToForm();

    this.form.valueChanges.subscribe(() => {
      this.validityChanges.emit(this.form.valid);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.checkCredentials();
    this.credentialsToForm();
  }

  getCredentials(): RedditSourceCredentials {
    if (!this.form.valid) {
      return;
    }

    this.formToCredentials();

    return this.credentials;
  }

  resetCredentials(): void {
  }

  setCredentials(credentials: RedditSourceCredentials): void {
    this.credentials = credentials;
    this.credentialsToForm();
  }

  formToCredentials(): void {
    assign(this.credentials, this.form.getRawValue());
  }

  credentialsToForm(): void {
    this.form.patchValue(this.credentials);
  }

  checkCredentials(): void {
    this.credentials.type = 'reddit';
    this.credentials.userAgent = this.credentials.userAgent || 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0';
    this.credentials.clientId = this.credentials.clientId || '';
    this.credentials.clientSecret = this.credentials.clientSecret || '';
    this.credentials.refreshToken = this.credentials.refreshToken || '';
    this.credentials.username = this.credentials.username || '';
    this.credentials.password = this.credentials.password || '';
  }
}
