import {RunConfig} from '../RunConfig';
import {Item} from '../../generic/Item';
import {Media} from '../../generic/Media';
import {Observable, Subject} from 'rxjs';
import {SourceModule} from './SourceModule';
import {AnalyseModule} from './AnalyseModule';
import {DownloadModule} from './DownloadModule';

export enum RunStepType {
  Source,
  Analyse,
  Download,
}

export enum RunStepState {
  Waiting,
  Running,
  Finished,
}

export enum RunStepError {
  Unknown,
  Network,
  NoModule,
}

export interface RunStepProgress {
  percentage: number;
  state: RunStepState;
  error?: RunStepError;
  started: number;
  finished: number;
  update: Subject<void>;
}

export interface RunStep {
  progress: RunStepProgress;
}

export interface RunStepSource extends RunStep {
  module: SourceModule;
  output: Item[];
}

export interface RunStepAnalyse extends RunStep {
  input: Item;
  module: AnalyseModule;
  output: Media[];
}

export interface RunStepDownload extends RunStep {
  input: Media;
  module: DownloadModule;
  output: RunMedia[];
}

export interface RunProgress extends RunStepProgress {
  step: RunStepType;
}

export enum RunStepModuleWeight {
  Minimal,
  Low,
  Normal,
  High,
  Critical,
}

export interface RunStepModuleDefinition {
  name: string;
  description: string;
  type: string;
  weight: RunStepModuleWeight;
  accepts: (run: Run) => boolean;
  instantiate: (run: Run) => RunStepModule;
}

export interface RunHelpers {
  loadItem: (id: string) => Observable<Item>;
}

export interface Run {
  configs: RunConfig;
  helpers: RunHelpers;
  progress: RunProgress;
  steps: {
    source: RunStepSource;
    analyse: RunStepAnalyse[];
    download: RunStepDownload[];
  };
}

export interface RunMedia {
  media: Media;
  data: any;
}

export interface RunStepModule {
  definition: RunStepModuleDefinition;
  run(): Observable<void>;
}

export interface RunStepSourceModule extends RunStepModule {
  addStepAnalyse(step: RunStepAnalyse): void;
}

export interface RunStepAnalyseModule extends RunStepModule {
  addStepDownload(step: RunStepDownload);
}

export interface RunStepDownloadModule extends RunStepModule {
}
