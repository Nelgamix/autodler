import {Runner, RunnerSettings} from './Runner';
import {ContainerStepModule} from '../step/module/ContainerStepModule';
import {SourceItem} from '../item/SourceItem';
import {AppService} from '../../../../providers/app.service';
import {ContainerStepItems} from '../step/item/ContainerStepItems';
import {Context} from '../Context';
import {StepModuleDefinition} from '../step/module/definition/StepModuleDefinition';

const modules: StepModuleDefinition<ContainerStepModule<any>, SourceItem>[] = [];

export class ContainerRunner extends Runner<SourceItem, ContainerStepItems> {
  constructor(app: AppService, context: Context, settings: RunnerSettings) {
    super(modules, app, context, settings);
  }
}
