import {Picker} from './Picker';
import {NoModuleSource} from '../../../modules/source/NoModuleSource';
import {RedditSource} from '../../../modules/source/reddit/RedditSource';
import {SourceConfiguration} from '../configuration/SourceConfiguration';
import {SourceStepModule} from '../step/module/SourceStepModule';
import {StepModuleDefinition} from '../step/module/definition/StepModuleDefinition';

type ModuleType = StepModuleDefinition<SourceStepModule<any, any>, SourceConfiguration>;

const modulesAvailable: ModuleType[] = [
  NoModuleSource.definition,
  RedditSource.definition,
];

export class SourcePicker extends Picker<ModuleType> {
  constructor(modules: ModuleType[]) {
    super(modules);
  }

  pick(): ModuleType | undefined {
    return undefined;
  }
}

export const sourcePicker = new SourcePicker(modulesAvailable);
