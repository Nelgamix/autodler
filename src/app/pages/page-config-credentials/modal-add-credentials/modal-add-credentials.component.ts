import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalRef} from 'ng-zorro-antd';
import {Credentials} from '../../../logic/interfaces/generic/Credentials';

@Component({
  selector: 'app-modal-add-credentials',
  templateUrl: './modal-add-credentials.component.html',
  styleUrls: ['./modal-add-credentials.component.scss']
})
export class ModalAddCredentialsComponent implements OnInit {
  @Input() credentials: Credentials;

  form: FormGroup;
  types = [['Reddit', 'reddit']];

  constructor(
    private modal: NzModalRef,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    if (!this.credentials) {
      this.credentials = {
        name: '',
        type: '',
      };
    }

    this.form = this.fb.group({
      name: [this.credentials.name, [Validators.required]],
      type: [this.credentials.type, [Validators.required]],
    });
  }

  handleCancel(): void {
    this.modal.close();
  }

  handleOk(): void {
    if (!this.form.valid) {
      return;
    }

    this.copyFormInCredentials();

    this.modal.close(this.credentials);
  }

  copyCredentialsInForm(): void {
    this.form.controls['name'].patchValue(this.credentials.name);
    this.form.controls['type'].patchValue(this.credentials.type);
  }

  copyFormInCredentials(): void {
    this.credentials.name = this.form.controls['name'].value;
    this.credentials.type = this.form.controls['type'].value;
  }
}
