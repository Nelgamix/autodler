import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {concat, Observable, of} from 'rxjs';
import {last, mapTo, switchMap} from 'rxjs/operators';
import {AppService} from '../../../providers/app.service';
import {Config} from '../../../logic/interfaces/generic/Config';
import {Index} from '../../../logic/interfaces/generic';
import {
  DownloadItem,
  DownloadItemRunState,
  DownloadItems,
  DownloadMedia,
} from '../../../logic/interfaces/application/download/DownloadItems';
import {SourceItems} from '../../../logic/interfaces/application/source/SourceItems';
import {DownloadError} from '../../../logic/interfaces/application/download/DownloadInfo';
import {Downloader} from '../../../logic/process/Downloader';

@Component({
  selector: 'app-run-download',
  templateUrl: './run-download.component.html',
  styleUrls: ['./run-download.component.scss']
})
export class RunDownloadComponent implements OnInit {
  @Input() config: Config;
  @Input() index: Index;
  @Input() sourceItems: SourceItems;
  @Input() allowedRun: boolean;
  downloadItems: DownloadItems;
  running = false;
  @Output() runStarted = new EventEmitter<void>();
  @Output() mediaDownloaded = new EventEmitter<DownloadMedia>();
  @Output() itemDownloaded = new EventEmitter<DownloadItem>();
  @Output() runFinished = new EventEmitter<DownloadItems>();

  DownloadError = DownloadError;

  private downloader: Downloader;

  constructor(
    private app: AppService,
  ) { }

  ngOnInit() {
  }

  run(): void {
    if (!this.canRun || this.running) {
      return;
    }

    this.reset();

    this.running = true;
    this.downloadItems = this.app.downloadService.run(this.sourceItems);
    this.downloader = new Downloader(this.downloadItems);

    this.launchAnalyse();

    this.runStarted.emit();
  }

  openBrowser(url: string): void {
    if (this.app.electronService.isElectron()) {
      this.app.electronService.shell.openExternal(url);
    }
  }

  pause(): void {
    this.downloader.pause();
  }

  resume(): void {
    this.downloader.resume();
  }

  reset(): void {
    this.running = false;
    this.downloadItems = null;
    this.downloader = null;
  }

  showProgress(item: DownloadItem): boolean {
    return item.run.state === DownloadItemRunState.Running
      || (item.run.state === DownloadItemRunState.Finished && !item.input.downloadInfo.error);
  }

  get canRun(): boolean {
    return this.allowedRun
      && !this.running
      && Boolean(this.index)
      && Boolean(this.sourceItems)
      && this.sourceItems.output.filter(i => i.downloadInfo.marked).length > 0;
  }

  get itemsMarked(): DownloadItem[] {
    return this.downloadItems.output.filter(item => item.input.downloadInfo.marked);
  }

  private createFolders(): Observable<void> {
    return concat(...[
      this.app.electronService.existsFile(this.config.paths.medias).pipe(
        switchMap((res) => !res ? this.app.electronService.createDirectory(this.config.paths.medias) : of(true)),
      ),
      this.app.electronService.existsFile(this.config.paths.items).pipe(
        switchMap((res) => !res ? this.app.electronService.createDirectory(this.config.paths.items) : of(true)),
      ),
    ]).pipe(
      last(),
      mapTo(null),
    );
  }

  private launchAnalyse(): void {
    // Create folder of items
    this.createFolders().subscribe(() => {
      this.downloader.analyseFinished.subscribe(() => this.launchRun());
      this.downloader.analyse();
    });
  }

  private launchRun(): void {
    this.downloader.downloadMediaFinished.subscribe(this.mediaDownloaded);
    this.downloader.downloadItemFinished.subscribe(this.itemDownloaded);
    this.downloader.downloadFinished.subscribe(this.runFinished);

    this.downloader.download();
  }
}
