import {Component, OnInit, ViewChild} from '@angular/core';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';
import {assign} from 'lodash';
import {AbstractCredentials} from '../../components/sources/AbstractCredentials';
import {ModalAddCredentialsComponent} from './modal-add-credentials/modal-add-credentials.component';
import {AppService} from '../../providers/app.service';
import {Workspace} from '../../logic/interfaces/generic/Workspace';
import {Credentials} from '../../logic/interfaces/generic/Credentials';

@Component({
  selector: 'app-page-config-credentials',
  templateUrl: './page-config-credentials.component.html',
  styleUrls: ['./page-config-credentials.component.scss']
})
export class PageConfigCredentialsComponent implements OnInit {
  @ViewChild('configurator', {static: false}) configurator: AbstractCredentials;

  workspace: Workspace;
  formValid = false;

  credentialsSelected: Credentials;

  constructor(
    private app: AppService,
    private modalService: NzModalService,
    private message: NzMessageService,
  ) { }

  ngOnInit() {
    this.workspace = this.app.workspace;

    // Select by default
    if (this.credentials.length > 0) {
      this.selectCredentials(this.credentials[0]);
    }
  }

  selectCredentials(credentials: Credentials): void {
    this.credentialsSelected = credentials;
  }

  showModal(credentials?: Credentials): void {
    const modal = this.modalService.create({
      nzTitle: 'Add credentials',
      nzContent: ModalAddCredentialsComponent,
      nzComponentParams: {
        credentials,
      }
    });

    modal.afterClose.subscribe((result: Credentials | undefined) => {
      if (result) {
        if (this.workspace.credentials.find(c => c === result)) {
          // Exists
          this.saveCredentials();
        } else {
          this.workspace.credentials.push(result);
        }
      }
    });
  }

  saveCredentials(): void {
    this.formValid = false;
    assign(this.credentialsSelected, this.configurator.getCredentials());
    this.app.workspaceService.saveWorkspace(this.workspace).subscribe(() => {
      this.message.success('Your credentials has been successfully saved.');
    });
  }

  deleteCredentials(credentials: Credentials): void {
    const idx = this.workspace.credentials.findIndex(c => c === credentials);
    if (idx >= 0) {
      this.workspace.credentials.splice(idx, 1);
    }

    if (this.credentials.length > 0) {
      this.selectCredentials(this.credentials[0]);
    } else {
      this.credentialsSelected = undefined;
    }
  }

  validityChanges(newValidity: boolean): void {
    this.formValid = newValidity;
  }

  get credentials(): Credentials[] {
    return this.workspace.credentials;
  }
}
