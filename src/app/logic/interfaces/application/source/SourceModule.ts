import {Observable} from 'rxjs';
import {SourceModuleDefinition} from './SourceModuleDefinition';
import {RunConfig} from '../RunConfig';
import {Index} from '../../generic';
import {Item} from '../../generic/Item';
import {SourceItems, SourceItemsRunState} from './SourceItems';

export abstract class SourceModule {
  public sourceItems: SourceItems;

  protected definition: SourceModuleDefinition;

  static newIndex(path: string): Index {
    return {
      folder: path,
      items: {
        success: [],
        errors: [],
        unmarked: [],
      },
    };
  }

  protected constructor(protected runConfig: RunConfig) {
    this.sourceItems = this.constructOutput();
  }

  public abstract run(): Observable<SourceItemsRunState>;

  public getPathForMedia(item: Item): string {
    return this.runConfig.config.paths.medias;
  }

  protected constructOutput(): SourceItems {
    return {
      output: [],
      config: this.runConfig,
      module: this,
      run: {
        start: null,
        progress: 0,
        state: SourceItemsRunState.Waiting,
      },
    };
  }

  protected constructItem(id?: string, title?: string, url?: string): Item {
    return {
      path: this.runConfig.config.paths.items + (id ? '/' + id + '.json' : ''),
      id: id || '',
      type: this.definition.type,
      created: Date.now(),
      title: title || '',
      url: url,
      medias: [],
      info: {
        marked: true,
        analyse: {
          done: false,
        },
        download: {
          done: false,
        },
      },
    };
  }

  protected initRun(): void {
    this.sourceItems.run.start = this.run();
  }
}
